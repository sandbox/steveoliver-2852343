<?php

namespace Drupal\sba_geodata;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Exception\ClientException;

class Geodata {

  protected $client;

  /**
   * Constructs a new CaCountyCityData object.
   *
   * @param \Drupal\sba_geodata\ClientFactory $client_factory
   *   The client factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(ClientFactory $client_factory, LoggerChannelFactoryInterface $logger_factory) {
    $this->client = $client_factory->createInstance();
    $this->logger = $logger_factory->get('sba_geodata');
  }

  /**
   * Gets city data for a given state.
   *
   * @param string $state
   *   The two-letter postal code for the state.
   * @param string $urls
   *   'all': For all URLs.
   *   'primary': For primary URLs only.
   *   'any': For all data with or without URLs.
   *
   * @return array
   *   An array of response data.
   *
   * @throws \Exception|\GuzzleHttp\Exception\ClientException
   */
  public function StateCityData($state, $urls = 'any') {
    switch ($urls) {
      case 'all':
        $path = 'city_links_for_state_of/';
        break;
      case 'primary':
        $path = 'primary_city_links_for_state_of/';
        break;
      case 'any':
      default:
        $path = 'city_data_for_state_of/';
        break;
    }
    $path .= $state . '.json';
    try {
      $response = $this->client->get($path);
      return json_decode($response->getBody()->getContents(), TRUE);
    }
    catch (ClientException $e) {
      // @todo port error handling from D7.
      $this->logger->error($e->getResponse()->getBody()->getContents());
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
  }

}
