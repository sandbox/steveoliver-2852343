<?php

namespace Drupal\sba_geodata;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Http\ClientFactory as CoreClientFactory;

class ClientFactory {

  protected $clientFactory;

  /**
   * Constructs a new SBA geodata ClientFactory object.
   *
   * @param \Drupal\Core\Http\ClientFactory $client_factory
   *   The client factory.
   */
  public function __construct(CoreClientFactory $client_factory) {
    $this->clientFactory = $client_factory;
  }

  /**
   * Gets an API client instance.
   *
   * @param array $config
   *   Additional config for the client.
   *
   * @return \GuzzleHttp\Client
   *   The API client.
   */
  public function createInstance(array $config = []) {
    $default_config = [
      'base_uri' => 'http://api.sba.gov/geodata/',
    ];
    $config = NestedArray::mergeDeep($default_config, $config);
    return $this->clientFactory->fromOptions($config);
  }

}
